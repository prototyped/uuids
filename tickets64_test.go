// Copyright (C) 2019 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the MIT License.
// See accompanying files LICENSE.

// +build !ignore

package uuid

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"sync"
	"testing"
	"time"
)

var (
	host   = "127.0.0.1"
	port   = 3306
	user   = "root"
	passwd = "holyshit"
	db     = "uuid"
)

func makeDSN() string {
	format := `%s:%s@tcp(%s:%d)/%s?charset=utf8&interpolateParams=true&parseTime=true&charset=utf8&loc=Local`
	return fmt.Sprintf(format, user, passwd, host, port, db)
}

func TestTicket64Example(t *testing.T) {
	var gen = Ticket64{}
	if err := gen.Init("mysql", makeDSN(), "a"); err != nil {
		t.Fatalf("MySQL: %v", err)
	}
	defer gen.Close()
	for i := 0; i < 100; i++ {
		_, err := gen.Next()
		if err != nil {
			t.Fatalf("Next: %v", err)
		}
	}
}

func TestTicket64Concurrency(t *testing.T) {
	var gen = Ticket64{}
	if err := gen.Init("mysql", makeDSN(), "b"); err != nil {
		t.Fatalf("MySQL: %v", err)
	}
	defer gen.Close()

	var store = make(map[uint64]bool)
	var bus = make(chan uint64, 4000000)
	var wg sync.WaitGroup

	var worker = func(i int) {
		defer wg.Done()
		var ticker = time.NewTicker(time.Second)
		for {
			select {
			case <-ticker.C:
				return
			default:
				var uuid = gen.MustNext()
				//println("worker", i, uuid)
				bus <- uuid
			}
		}
	}
	var concurrency = 4
	for i := 1; i <= concurrency; i++ {
		wg.Add(1)
		go worker(i)
	}

	//
	go func() {
		for uuid := range bus {
			if _, found := store[uuid]; found {
				t.Fatalf("uuid already exist %d", uuid)
			}
			store[uuid] = true
		}
	}()

	wg.Wait()
	close(bus)

	fmt.Printf("Ticket64 can generate %d uuids per second with %d concurrency\n", len(store), concurrency)
}

func BenchmarkTicket64(b *testing.B) {
	b.StopTimer()
	var gen = Ticket64{}
	if err := gen.Init("mysql", makeDSN(), "c"); err != nil {
		b.Fatalf("MySQL: %v", err)
	}
	defer gen.Close()
	b.StartTimer()
	var total uint64
	for i := 0; i < b.N; i++ {
		n, err := gen.Next()
		if err != nil {
			b.Fatalf("Next: %v", err)
		}
		total += n
	}
}
