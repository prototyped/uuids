// Copyright (C) 2019 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the MIT License.
// See accompanying files LICENSE.

package uuid

import (
	"database/sql"
	"log"
	"sync"
)

type Sequence struct {
	sync.Mutex
	db     *sql.DB
	id     int
	step   int
	curSeq int64
	maxSeq int64
}

func (s *Sequence) Init(driver, dsn string, id, step int) error {
	s.id = id
	s.step = step
	if s.step <= 0 {
		s.step = 10000
	}
	db, err := sql.Open(driver, dsn)
	if err != nil {
		return err
	}
	if err = db.Ping(); err != nil {
		return err
	}
	s.db = db
	if err = s.createTable(); err != nil {
		return err
	}
	if err = s.load(); err != nil {
		return err
	}
	return nil
}

func (s *Sequence) Close() {
	s.Lock()
	if s.db != nil {
		s.db.Close()
		s.db = nil
	}
	s.Unlock()
}

func (s *Sequence) Next() (int64, error) {
	s.Lock()
	s.curSeq++
	if s.curSeq >= s.maxSeq {
		s.maxSeq += int64(s.step)
		if err := s.store(); err != nil {
			s.Unlock()
			return 0, err
		}
	}
	s.Unlock()
	return s.curSeq, nil
}

func (s *Sequence) MustNext() int64 {
	if n, err := s.Next(); err != nil {
		log.Panicf("Sequence.Next: %v", err)
	} else {
		return n
	}
	return 0
}

func (s *Sequence) store() error {
	var stmt = "UPDATE `seq` SET `cur_seq`=?, `max_seq`=? WHERE `id`=?"
	_, err := s.db.Exec(stmt, s.curSeq, s.maxSeq, s.id)
	return err
}

func (s *Sequence) load() error {
	var stmt = "SELECT `cur_seq`, `max_seq` FROM `seq` WHERE `id`=?"
	var err = s.db.QueryRow(stmt, s.id).Scan(&s.curSeq, &s.maxSeq)
	if err == nil {
		return nil
	}
	if err == sql.ErrNoRows {
		stmt = "INSERT INTO `seq`(`id`, `cur_seq`, `max_seq`) VALUES(?,?,?)"
		_, err = s.db.Exec(stmt, s.id, 1000, s.step+1000) // start from 1000
		return err
	}
	return err
}

func (s *Sequence) createTable() error {
	var sql = "CREATE TABLE IF NOT EXISTS `seq` (" +
		"`id` INT(11) NOT NULL AUTO_INCREMENT," +
		"`cur_seq` BIGINT(20) NOT NULL, " +
		"`max_seq` BIGINT(20) NOT NULL," +
		"PRIMARY KEY (`id`));"
	_, err := s.db.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}
