// Copyright (C) 2019 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the MIT License.
// See accompanying files LICENSE.

package uuid

import (
	"database/sql"
	"fmt"
	"log"
	"sync"
)

type Ticket64 struct {
	mut  sync.Mutex
	key  string
	db   *sql.DB
	stmt *sql.Stmt
}

func (t *Ticket64) Init(driver, dsn, key string) error {
	t.key = key
	db, err := sql.Open(driver, dsn)
	if err != nil {
		return err
	}
	if err = db.Ping(); err != nil {
		return err
	}
	t.db = db
	if err = t.createTable(); err != nil {
		return err
	}
	stmt, err := t.db.Prepare(fmt.Sprintf("REPLACE INTO `ticket` (stub) VALUES ('%s')", t.key))
	if err != nil {
		return err
	}
	t.stmt = stmt
	return nil
}

func (t *Ticket64) Close() {
	t.mut.Lock()
	defer t.mut.Unlock()
	if t.stmt != nil {
		t.stmt.Close()
		t.stmt = nil
	}
	if t.db != nil {
		t.db.Close()
		t.db = nil
	}
}

func (t *Ticket64) Next() (uint64, error) {
	t.mut.Lock()
	result, err := t.stmt.Exec()
	if err != nil {
		t.mut.Unlock()
		return 0, err
	}
	n, err := result.LastInsertId()
	if err != nil {
		t.mut.Unlock()
		return 0, err
	}
	t.mut.Unlock()
	return uint64(n), nil
}

func (t *Ticket64) MustNext() uint64 {
	if n, err := t.Next(); err != nil {
		log.Panicf("Ticket64.Next: %v", err)
	} else {
		return n
	}
	return 0
}

func (t *Ticket64) createTable() error {
	var sql = "CREATE TABLE IF NOT EXISTS `ticket` (" +
		"`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT," +
		"`stub` CHAR(4) NOT NULL, " +
		"PRIMARY KEY (`id`), " +
		"UNIQUE INDEX `stub` (`stub`)" +
		")" +
		"AUTO_INCREMENT=10000;"

	_, err := t.db.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}
