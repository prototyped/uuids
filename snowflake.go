// Copyright (C) 2018 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the MIT License.
// See accompanying files LICENSE.


// Snowflake, Twitter's distributed unique ID generator.
//
// A Snowflake ID is composed of
//     41 bits for timestamp in milliseconds
//     12 bits for a machine id
//     10 bits for a sequence number
package uuid

import (
	"log"
	"sync"
	"time"
)

// These constants are the bit lengths of Snowflake ID parts.
const (
	DefaultSequenceBits  = 10
	DefaultMachineIDBits = 12
	MachineSeqBitsLimit  = 22

	Twepoch = 946684800000000000 // custom epoch in nanosecond, (2000-01-01 00:00:00 UTC)
)

// Snowflake with locking
type Snowflake struct {
	sync.Mutex
	sequence       uint64 // last sequence
	lastTimestamp  uint64 // last timestamp
	lastID         uint64 // last generated id
	ticks          uint64 //
	sequenceShift  uint16 // bits of sequence no. take
	timestampShift uint16 // bits to shift timestamp
	machineID      uint16 // id of this machine(process)
}

func currentTimeUnit() uint64 {
	return uint64(time.Now().UTC().UnixNano()-Twepoch) / 1e6 // nanoseconds to milliseconds
}

// sequence + machineID should less than 22 bits
func NewSnowflake(sequenceBits, machineIDBits, machineId uint16) *Snowflake {
	if sequenceBits == 0 {
		sequenceBits = DefaultSequenceBits
	}
	if machineIDBits == 0 {
		machineIDBits = DefaultMachineIDBits
	}
	if int(sequenceBits+machineIDBits) > MachineSeqBitsLimit {
		log.Panicf("snowflake: <%d %d> bits too long", machineIDBits, sequenceBits)
	}

	var now = currentTimeUnit()
	return &Snowflake{
		sequenceShift:  sequenceBits,
		timestampShift: sequenceBits + machineIDBits,
		machineID:      machineId % ((1 << machineIDBits) - 1),
		lastTimestamp:  now,
	}
}

func (sf *Snowflake) Next() uint64 {
	sf.Lock()
	var ts = currentTimeUnit()
	if ts < sf.lastTimestamp {
		sf.Unlock()
		log.Panicf("Snowflake: time has gone backwards, %v -> %v, %v", ts, sf.lastTimestamp, sf.ticks)
	}
	if ts == sf.lastTimestamp {
		sf.sequence++
		if sf.sequence >= (1 << sf.sequenceShift) { // sequence expired, tick to next time unit
			sf.sequence = 0
			ts = tilNextTimeUnit(ts)
		}
	} else {
		sf.sequence = 0
	}
	sf.lastTimestamp = ts

	var id = (ts << uint64(sf.timestampShift)) | (uint64(sf.machineID) << uint64(sf.sequenceShift)) | sf.sequence
	if id <= sf.lastID {
		sf.Unlock()
		log.Panicf("SnowFlake: ID has gone backwards, %x -> %x, %x, %v", id, sf.lastID, ts, sf.ticks)
	}

	sf.lastID = id
	sf.ticks++
	sf.Unlock()

	return id
}

// tick to next
func tilNextTimeUnit(ts uint64) uint64 {
	for {
		time.Sleep(time.Millisecond)
		var now = currentTimeUnit()
		if now > ts {
			return now
		}
	}
}
