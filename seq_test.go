// Copyright (C) 2019 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the MIT License.
// See accompanying files LICENSE.

package uuid

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

func TestSeqExample(t *testing.T) {
	var seq = Sequence{}
	if err := seq.Init("mysql", makeDSN(), 101, 10000); err != nil {
		t.Fatalf("MySQL: %v", err)
	}
	defer seq.Close()
	for i := 0; i < 100; i++ {
		_, err := seq.Next()
		if err != nil {
			t.Fatalf("Next: %v", err)
		}
	}
}

func TestSeqConcurrency(t *testing.T) {
	var seq = Sequence{}
	if err := seq.Init("mysql", makeDSN(), 102, 100000); err != nil {
		t.Fatalf("MySQL: %v", err)
	}
	defer seq.Close()

	var store = make(map[int64]bool)
	var bus = make(chan int64, 4000000)
	var wg sync.WaitGroup

	var worker = func(i int) {
		defer wg.Done()
		var ticker = time.NewTicker(time.Second)
		for {
			select {
			case <-ticker.C:
				return
			default:
				var uuid = seq.MustNext()
				//println("worker", i, uuid)
				bus <- uuid
			}
		}
	}
	var concurrency = 4
	for i := 1; i <= concurrency; i++ {
		wg.Add(1)
		go worker(i)
	}

	//
	go func() {
		for uuid := range bus {
			if _, found := store[uuid]; found {
				t.Fatalf("uuid already exist %d", uuid)
			}
			store[uuid] = true
		}
	}()

	wg.Wait()
	close(bus)

	fmt.Printf("Sequence can generate %d uuids per second with %d concurrency\n", len(store), concurrency)
}
