// Copyright (C) 2018 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the MIT License.
// See accompanying files LICENSE.

// +build ignore

package uuid

import (
	"fmt"
	"strconv"
	"strings"
	"sync"
	"testing"
	"time"
)

func TestSnowFlakeLimitation(t *testing.T) {
	var sf = NewSnowflake(0, 0, 1234)
	var uuid = sf.Next()
	t.Logf("a uuid is %v", uuid)

	var maxLimitYearBinary = strings.Repeat("1", 64-int(sf.timestampShift))
	maxTimestamp, err := strconv.ParseInt(maxLimitYearBinary, 2, 64)
	if err != nil {
		t.Errorf("ParseInt: %v", err)
	}
	var epoch = time.Unix(0, Twepoch)
	var total = float64(maxTimestamp/100-epoch.Unix()) / (86400 * 365)
	t.Logf("default snowfake will exhoused after %.4f years", total)
}

func TestSnowFlakeConcurrency(t *testing.T) {
	var sf = NewSnowflake(0, 0, 1234)
	var store = make(map[uint64]bool)
	var bus = make(chan uint64, 4000000)
	var wg sync.WaitGroup

	var worker = func(i int) {
		defer wg.Done()
		var ticker = time.NewTicker(time.Second)
		for {
			select {
			case <-ticker.C:
				return
			default:
				var uuid = sf.Next()
				//println("worker", i, uuid)
				bus <- uuid
			}
		}
	}
	var concurrency = 4
	for i := 1; i <= concurrency; i++ {
		wg.Add(1)
		go worker(i)
	}

	//
	go func() {
		for uuid := range bus {
			if _, found := store[uuid]; found {
				t.Fatalf("uuid already exist %d", uuid)
			}
			store[uuid] = true
		}
	}()

	wg.Wait()
	close(bus)

	fmt.Printf("Snowflake can generate %d uuids per second with %d concurrency\n", len(store), concurrency)
}

func BenchmarkSnowflake(b *testing.B) {
	var sf = NewSnowflake(0, 0, 1234)
	for i := 0; i < b.N; i++ {
		sf.Next()
	}
}
